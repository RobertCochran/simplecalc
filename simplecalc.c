/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Robert Cochran
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct Token {
	char *token;
	struct Token *next;
};

struct Stack {
	double num;
	struct Stack *next;
};

/* Push a token onto the token list */

void add_token(struct Token **tokens, char *new_token)
{
	struct Token *cur, *new;

	new = malloc(sizeof(struct Token));

	if (!new) {
		perror("Could not allocate token list member");
		exit(-1);
	}

	memset(new, 0, sizeof(struct Token));

	new->token = new_token;

	if (!*tokens) {
		/* No token list - create it */
		*tokens = new;
		return;
	}

	for (cur = *tokens; cur->next; cur = cur->next);
	cur->next = new;

	return;
}

/* Split an input string by spaces and add them to the token list */

void tokenize(struct Token **tokens, char *input)
{
	char *t = NULL;

	for (t = strtok(input, " "); t; t = strtok(NULL, " "))
		add_token(tokens, t);
}

/* Free the allocated memory of the token list */

void free_tokens(struct Token *tokens)
{
	struct Token *cur;

	for (; tokens->next; cur = tokens, tokens = tokens->next, free(cur));

	free(tokens);
}

/* Print a list of the numbers on the stack */

void dump_stack(struct Stack *stack)
{
	struct Stack *tmp;
	int i;

	printf("-----\nStack dump\n");

	for (tmp = stack, i = 0; tmp; tmp = tmp->next, i++)
		printf("%d - %lf\n", i, tmp->num);

	printf("-----\n");
}

/* Add a number to the stack */

void push_to_stack(struct Stack **stack, double n)
{
	struct Stack *new, *old;

	new = malloc(sizeof(struct Stack));

	if (!new) {
		perror("Could not allocate new stack member");
		exit(-1);
	}

	memset(new, 0, sizeof(struct Stack));

	old = *stack;
	*stack = new;
	(*stack)->num = n;
	(*stack)->next = old;

	return;
}

/* Remove and return the most recently pushed number on the stack */

double pop_from_stack(struct Stack **stack)
{
	if (!*stack) return 0;

	struct Stack *last;

	double ret = (*stack)->num;

	last = *stack;
	*stack = (*stack)->next;

	if (last) free(last);

	return ret;
}

/* Free the allocated memory of the stack */

void free_stack(struct Stack *stack)
{
	struct Stack *cur;

	for (; stack->next; cur = stack, stack = stack->next, free(cur));

	free(stack);
}

int main(void)
{
	char *input_buffer = malloc(1024 * sizeof(char));

	if (!input_buffer) {
		perror("Could not allocate input buffer");
		exit(-1);
	}

	struct Token *tokens = NULL;
	struct Stack *stack = NULL;
	int run = 1;

	while (run) {
		if (!fgets(input_buffer, 1024 * sizeof(char), stdin)) {
			perror("Error reading input");
			exit(-1);
		}

		tokenize(&tokens, input_buffer);

		struct Token *p;

		for (p = tokens; p; p = p->next) {
			switch (p->token[0]) {
			case '+':
				push_to_stack(&stack, pop_from_stack(&stack) +
					     pop_from_stack(&stack));
				break;
			case '-':
				push_to_stack(&stack, pop_from_stack(&stack) -
					     pop_from_stack(&stack));
				break;
			case '*':
				push_to_stack(&stack, pop_from_stack(&stack) *
					     pop_from_stack(&stack));
				break;
			case '/':
				push_to_stack(&stack, pop_from_stack(&stack) /
					     pop_from_stack(&stack));
				break;
			case 'p':
				printf("%lf\n\n", pop_from_stack(&stack));
				break;
			case 's':
				dump_stack(stack);
				break;
			case 'q':
				run = 0;
				break;
			default:
				push_to_stack(&stack, strtod(p->token, NULL));
				break;
			}
		}

		free_tokens(tokens);
		tokens = NULL;
	}

	free(input_buffer);

	if (stack) free_stack(stack);

	return 0;
}
