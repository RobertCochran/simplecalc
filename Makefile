# Makefile - build script

# Build flags
BASEFLAGS   := -O2 -fpic -pedantic
BASEFLAGS   += -fomit-frame-pointer -lgcc
WARNFLAGS   := -Wall -Wextra -Wshadow -Wcast-align -Wwrite-strings
WARNFLAGS   += -Winline  -Wno-attributes -Wno-deprecated-declarations
WARNFLAGS   += -Wno-div-by-zero -Wno-endif-labels -Wfloat-equal
WARNFLAGS   += -Wformat=2 -Wno-format-extra-args -Winit-self
WARNFLAGS   += -Winvalid-pch -Wmissing-format-attribute
WARNFLAGS   += -Wmissing-include-dirs -Wno-multichar
WARNFLAGS   += -Wshadow -Wno-sign-compare -Wswitch -Wsystem-headers -Wundef
WARNFLAGS   += -Wno-pragmas -Wno-unused-but-set-parameter
WARNFLAGS   += -Wno-unused-but-set-variable -Wno-unused-result
WARNFLAGS   += -Wwrite-strings -Wdisabled-optimization -Wpointer-arith
CFLAGS      := $(INCLUDES) $(DEPENDFLAGS) $(BASEFLAGS) $(WARNFLAGS)

# build rules
all: simplecalc

simplecalc: simplecalc.c
	$(CC) $(CFLAGS) -o simplecalc simplecalc.c

debug:
	$(CC) $(CFLAGS) -g -o simplecalc-debug simplecalc.c

clean:
	rm -rf *.o *~ \#*# \#* simplecalc simplecalc-debug
