simplecalc - simplistic RPN calculator
-----

Explanation
-----
simplecalc is a small program that can interpret Reverse Polish Notation
math and evaluate the results. simplecalc only knows the four basic operators
('+', '-', '*', '/'), so it isn't really suitable for anything particularly
complex, equasion-wise. simplecalc also knows the commands 'p', which pops
from the top of the number stack and displays it, 's', which dumps the stack,
and 'q', which quits simplecalc. simplecalc evaluates from left to right,
and math operators will use the newest stack member as a first argument,
then the second newest. For example, if you want to know what '5 / 2' is,
you would type '2 5 / p', which places 2 on the stack, places 5 over top of it,
then the division operator will pop 5 from the top as the first argument,
then 2 as the second, then perform the math and place '2.5' onto the stack,
which the 'p' command will pop from the stack then display it. It's entirely
possible to chain operations together - '1 1 + 1 1 + + p ' will return 4.

How to build
-----
Use the `make` tool. Code is 100% ANSI C99; nothing more is needed.

Known bugs
-----
simplecalc does not do any error checking - it trusts you that anything that
is not '+', '-', '*', '/', 's', 'p', or 'q' is a number.